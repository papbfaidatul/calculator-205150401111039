package com.example.calculator205150401111039;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //inisiasi variabel
    TextView Result,ResultTemp;
    MaterialButton b0,b1,b2,b3,b4,b5,b6,b7,b8,b9;
    MaterialButton bDiv,bMultiple,bIs,bPlus,bMin;
    MaterialButton bOpen,bClose,bPlusMinus,bAC,bDecimal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //connecting to view
        Result = findViewById(R.id.Result);
        ResultTemp = findViewById(R.id.ResultTemp);

        assignId(b0,R.id.b0);
        assignId(b1,R.id.b1);
        assignId(b2,R.id.b2);
        assignId(b3,R.id.b3);
        assignId(b4,R.id.b4);
        assignId(b5,R.id.b5);
        assignId(b6,R.id.b6);
        assignId(b7,R.id.b7);
        assignId(b8,R.id.b8);
        assignId(b9,R.id.b9);

        assignId(bDiv,R.id.bDiv);
        assignId(bMultiple,R.id.bMultiple);
        assignId(bIs,R.id.bIs);
        assignId(bPlus,R.id.bPlus);
        assignId(bMin,R.id.bMin);

        assignId(bOpen,R.id.bOpen);
        assignId(bClose,R.id.bClose);
        assignId(bPlusMinus,R.id.bPlusMinus);
        assignId(bAC,R.id.bAC);
        assignId(bDecimal,R.id.bDecimal);
    }

    //definisikan method assignId
    void assignId(MaterialButton btn, int id){
        btn = findViewById(id);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        MaterialButton button = (MaterialButton) view;
        String buttonText = button.getText().toString();

        String input = ResultTemp.getText().toString();
        input = input+buttonText;

        String result = getResult(input);
        ResultTemp.setText(input);

        if(buttonText.equals("AC")){
            ResultTemp.setText("");
            Result.setText("0");
            return;
        }
        if(buttonText.equals("+/-")){
            ResultTemp.setText("-" + Result.getText());
            Result.setText("-" + Result.getText());
            return;
        }
        if(buttonText.equals("=")){
            ResultTemp.setText(Result.getText());
            return;
        }
        if(!result.equals("Error")){
            Result.setText(result);
        }
    }
    String getResult(String data){
        try {
            Context context = Context.enter();
            context.setOptimizationLevel(-1);

            Scriptable scriptable = context.initStandardObjects();
            String finalResult = context.evaluateString(scriptable,data,"Javascript", 1, null).toString();

            if(finalResult.endsWith(".0")){
                finalResult = finalResult.replace(".0","");
            }
            return finalResult;
        }
        catch (Exception e){
            return "Error";
        }

    }
}